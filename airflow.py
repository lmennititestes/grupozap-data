###!/usr/bin/env python
# coding: utf-8


### Operation Session counter modules
from multiprocessing import Process, Pool
import sys
from collections import Counter
import multiprocessing
import pandas as pd
import numpy as np
import time
import copy
import os
import urllib
import json

### Apache Airflow modules
import airflow
from airflow.models import DAG
from airflow.operators.python_operator import PythonOperator

def download_file(url, file_name):
    try: 
        urllib.request.urlretrieve(url, download_dir+'/'+file_name)
    except urllib.error.URLError as e:
        print(e.reason)
        if('[Errno -3] Temporary failure in name resolution' == str(e.reason)):
            print('This error means you are either calling a domain name that doesn’t exist, or your DNS resolution has failed on your server, or check the internect connection')      


def check_existing_file(file_name):
    files = os.listdir(download_dir)
    if file_name in files:
        return True
    else:
        return False


def check_url_content_size_file(file_name):
    try:
        result = urllib.request.urlopen('https://s3.amazonaws.com/grupozap-data-engineer-test/'+file_name)
        return result.headers['content-length']
    except urllib.error.URLError as e:
        print(e.reason)


def check_local_content_file_size(file_name):
    statinfo = os.stat(download_dir+'/'+file_name)
    return statinfo.st_size


def df_read_chunks_json(filename, compression):
    df = pd.DataFrame()
    iter_json = pd.read_json(download_dir+'/'+filename, compression=compression, lines=True, chunksize=100000)
    print('Reading dataframe dataframe in chunks...')
    for pd_json_chunk in iter_json:        
        df = pd.concat([df, pd_json_chunk], ignore_index=True)
    df_final_cleaned, df_final_cleaned_dict = dataframe_cleanup_concat(df)    
    return df_final_cleaned, df_final_cleaned_dict


def dataframe_cleanup_concat(dataframe):
    print('Cleaning dataframe...')
    df_final_cleaned = dataframe.drop(['anonymous_id', 'browser_family', 'name', 'os_family'], axis=1)
    df_final_cleaned_dict = df_final_cleaned.drop_duplicates(subset='device_family').set_index('device_family')
    df_final_cleaned_dict['device_sent_timestamp'] = 0
    df_final_cleaned_dict['session_number'] = 0
    df_final_cleaned_dict = df_final_cleaned_dict.to_dict()
    return df_final_cleaned, df_final_cleaned_dict


def session_counter(data_splitted, empty_result_dict):

    for index, row in data_splitted.iterrows():
        #check if device_sent_timestamp is bigger than 30 minutes
        if((row['device_sent_timestamp'] - empty_result_dict['device_sent_timestamp'][row['device_family']]) > 1800):
            empty_result_dict['session_number'][row['device_family']] = empty_result_dict['session_number'][row['device_family']] + 1 
            empty_result_dict['device_sent_timestamp'][row['device_family']] = row['device_sent_timestamp']
    return empty_result_dict


def download_files():
    processes = []

    starttime = time.time()

    for url in urls:
        file_name = url.rsplit('/', 1)[-1]
        if(check_existing_file(file_name)):
            if(str(check_url_content_size_file(file_name)) != str(check_local_content_file_size(file_name))):
                try:
                    os.remove(download_dir+'/'+file_name)
                    print('File removed {} '.format(file_name))
                except Exception as e:
                    print('Except {} '.format(e))
            else:
                print('File equal size, dont need to download {} '.format(file_name))
                continue
        p = multiprocessing.Process(target=download_file, args=(url,file_name))
        print('Downloading {}...'.format(file_name))
        processes.append(p)
        p.start()
    if not(processes == []):
        for process in processes:
            process.join()
    
    print('Download has been finished.')
    print('That took {} seconds'.format(time.time() - starttime))

def processing_files():
    processes = []
    starttime = time.time()
    files = os.listdir(download_dir)
    file_list = [filename for filename in files if filename.split('.')[1]=='json']
    file_list.sort()
    compression = 'gzip'
    final_dict_result = dict()

    print('Starting Processing Pool...')
    with Pool(processes=int((number_of_workers/2))) as pool_open_df:
        multiple_df_results = [pool_open_df.apply_async(df_read_chunks_json, (file_name, compression)) for file_name in file_list]
        for result in multiple_df_results:
            print('Starting a process...')
            data_splitted = np.array_split(result.get()[0], int((number_of_workers/2)))
            empty_result_dict = result.get()[1]         

            with Pool(processes=int((number_of_workers/2))) as pool_processing_results:
                worker_results = [pool_processing_results.apply_async(session_counter, (data_splitted[i], empty_result_dict)) for i in range(int((number_of_workers/2)))]
                print('Producing the final results...')
                for result in worker_results:
                    final_dict_result = Counter(final_dict_result) + Counter(result.get()['session_number'])
    pool_open_df.close()
    pool_open_df.join()
    pool_processing_results.close()
    pool_processing_results.join()
    print('Join all processes and close it...')
    
    print('Results has been finished')
    print('That took {} seconds'.format(time.time() - starttime))
    
    print('Saving final JSON file into {} path'.format(results_dir+'/'+'results.json'))
    result = json.dumps(final_dict_result)
    file = open(results_dir+'/'+'results.json','w')
    file.write(result)
    file.close()
    print('JSON file has been saved.')




args = {
    'owner': 'Menniti',
    'start_date': airflow.utils.dates.days_ago(2),
}

dag = DAG(
    dag_id='grupozap_data_session_counter',
    default_args=args,
    schedule_interval=None,
)

download_files_pyop = PythonOperator(
    task_id='download_files',
    python_callable=download_files,
    #op_kwargs={'random_base': float(i) / 10},
    dag=dag,
)

processing_files_pyop = PythonOperator(
    task_id='processing_files',
    provide_context=True,
    python_callable=processing_files,
    dag=dag,
)


##
urls = [
    'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00000.json.gz',
    'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00001.json.gz',
    'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00002.json.gz',
    'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00003.json.gz',
    'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00004.json.gz'
]

path = os.getcwd()

if not (os.path.isdir('./downloads')):
    os.mkdir('./downloads')
if not (os.path.isdir('./results')):
    os.mkdir('./results')

download_dir = './downloads'
results_dir = './results'
    
files = os.listdir(download_dir)

number_of_workers = multiprocessing.cpu_count()

download_files_pyop >> processing_files_pyop


