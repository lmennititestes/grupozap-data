# Luiz Menniti - Data Engineer

# Documentation

*PRE-REQUISITS*:
- Python3.5 +(Higher)
- Pip

### Download code
Using git:

```
cd ~/Desktop/myrepo$
git clone https://lmennititestes@bitbucket.org/lmennititestes/grupozap-data.git

```

### Settings

Open config.yml and change the path for, URL, downloads and results.
```
config:
    urls: [   
      'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00000.json.gz',
      'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00001.json.gz',
      'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00002.json.gz',
      'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00003.json.gz',
      'https://s3.amazonaws.com/grupozap-data-engineer-test/part-00004.json.gz'
      ]
    download_dir: "./downloads"
    results_dir: "./results"
```

### Setup Enviroment

Pip Instalation
```
curl https://bootstrap.pypa.io/get-pip.py -o get-pip.py

python get-pip.py

```

Requirements instalation using PIP

```
pip install -r requirements.txt
```
### Running code

```
python3 grupozap-data.py

```

### How it works

The download_files_main(url_args) call the process once per download.
It checks if the file is already on the ./downloads path, if not the file is downloaded.

The processing_files_main() create a pool_open_df of workers based on half CPU available.
The workers will process it asynchronously, opening up the json.gz file in chunks and transform it into a Dataframe.

The Dataframe will be cleaned, removing unecessary columns, add index and setup new columns and generate a dictionary of results without duplicated keys. Once the result of this Dataframe has been finished, it start to be processed by pool_processing_results's workers (other half of CPU available) and provide partial-final_result, the final result will be united into final result.
The final result will be saved as a json file into ./results path.

### Self-learning

* Shared data and debug threads is NOT easy task, pool of workers are easier to debug!

* Open up the files in chunks ALWAYS to avoid memory problems.

* Use the Spark next time to process the data, use multiprocessing only for testing or local needs.

* Docker **does not run multiple workers**, the suggestion is use one worker per docker and manage it with swarm. 

Checking it using: 

```
ps -aux
```

## THANKS
