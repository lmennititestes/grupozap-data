
from multiprocessing import Process, Pool
import sys
from collections import Counter
import yaml
import multiprocessing
import pandas as pd
import numpy as np
import time
import copy
import os
import urllib
import json


def download_file(url, file_name):
    try: 
        urllib.request.urlretrieve(url, download_dir+'/'+file_name)
    except urllib.error.URLError as e:
        print(e.reason)
        if('[Errno -3] Temporary failure in name resolution' == str(e.reason)):
            print('This error means you are either calling a domain name that doesn’t exist, or your DNS resolution has failed on your server, or check the internect connection')      


def check_existing_file(file_name):
    files = os.listdir(download_dir)
    if file_name in files:
        return True
    else:
        return False


def check_url_content_size_file(file_name):
    try:
        result = urllib.request.urlopen('https://s3.amazonaws.com/grupozap-data-engineer-test/'+file_name)
        return result.headers['content-length']
    except urllib.error.URLError as e:
        print(e.reason)


def check_local_content_file_size(file_name):
    statinfo = os.stat(download_dir+'/'+file_name)
    return statinfo.st_size



def df_read_chunks_json(filename, compression):
    df = pd.DataFrame()
    iter_json = pd.read_json(download_dir+'/'+filename, compression=compression, lines=True, chunksize=100000)
    print('Reading dataframe dataframe in chunks...')
    for pd_json_chunk in iter_json:        
        df = pd.concat([df, pd_json_chunk], ignore_index=True)
        ##TODO save copy in queue to work simultaneously.
        ##Collections Deque 
        ##Save each copy into other service no shared memory.
        ##Read it from each dataframe, open it as a process to compound a chunk into the multiprocessing module.
        ##OBS: It needs to be delivered sorted, session_counter() need it. 
        ##V2.0 - Implement it with multiprocessing
        ##Create Deque Array (receive from everybody) and sort it and fills up other once aother array pops up the maximum collection Sorted, 
        ##V3.0 - Implement with events streaming
        ##Spark
        ##Async Wait - Bizanc reference
    df_final_cleaned, df_final_cleaned_dict = dataframe_cleanup_concat(df)    
    return df_final_cleaned, df_final_cleaned_dict



def dataframe_cleanup_concat(dataframe):
    print('Cleaning dataframe...')
    df_final_cleaned = dataframe.drop(['anonymous_id', 'browser_family', 'name', 'os_family'], axis=1)
    df_final_cleaned_dict = df_final_cleaned.drop_duplicates(subset='device_family').set_index('device_family')
    df_final_cleaned_dict['device_sent_timestamp'] = 0
    df_final_cleaned_dict['session_number'] = 0
    df_final_cleaned_dict = df_final_cleaned_dict.to_dict()
    return df_final_cleaned, df_final_cleaned_dict
    


def session_counter(data_splitted, empty_result_dict):

    for index, row in data_splitted.iterrows():
        #check if device_sent_timestamp is bigger than 30 minutes
        if((row['device_sent_timestamp'] - empty_result_dict['device_sent_timestamp'][row['device_family']]) > 1800):
            empty_result_dict['session_number'][row['device_family']] = empty_result_dict['session_number'][row['device_family']] + 1 
            empty_result_dict['device_sent_timestamp'][row['device_family']] = row['device_sent_timestamp']
    return empty_result_dict
   

def download_files_main(urls):
    processes = []

    starttime = time.time()

    for url in urls:
        file_name = url.rsplit('/', 1)[-1]
        if(check_existing_file(file_name)):
            if(str(check_url_content_size_file(file_name)) != str(check_local_content_file_size(file_name))):
                try:
                    os.remove(download_dir+'/'+file_name)
                    print('File removed {} '.format(file_name))
                except Exception as e:
                    print('Except {} '.format(e))
            else:
                print('File equal size, dont need to download {} '.format(file_name))
                continue
        p = multiprocessing.Process(target=download_file, args=(url,file_name))
        print('Downloading {}...'.format(file_name))
        processes.append(p)
        p.start()
    if not(processes == []):
        for process in processes:
            process.join()
    
    print('Download has been finished.')
    print('That took {} seconds'.format(time.time() - starttime))



def processing_files_main():
    starttime = time.time()
    files = os.listdir(download_dir)
    file_list = [filename for filename in files if filename.split('.')[1]=='json']
    file_list.sort()
    compression = 'gzip'
    final_dict_result = dict()

    print('Starting Processing Pool...')
    with Pool(processes=int((number_of_workers/2))) as pool_open_df:
        multiple_df_results = [pool_open_df.apply_async(df_read_chunks_json, (file_name, compression)) for file_name in file_list]
        for result in multiple_df_results:
            print('Starting a process...')
            data_splitted = np.array_split(result.get()[0], int((number_of_workers/2)))
            empty_result_dict = result.get()[1]         

            with Pool(processes=int((number_of_workers/2))) as pool_processing_results:
                worker_results = [pool_processing_results.apply_async(session_counter, (data_splitted[i], empty_result_dict)) for i in range(int((number_of_workers/2)))]
                print('Producing the final results...')
                for result in worker_results:
                    final_dict_result = Counter(final_dict_result) + Counter(result.get()['session_number'])
    pool_open_df.close()
    pool_open_df.join()
    pool_processing_results.close()
    pool_processing_results.join()
    print('Join all processes and close it...')
    
    print('Results has been finished')
    print('That took {} seconds'.format(time.time() - starttime))
    
    print('Saving final JSON file into {} path'.format(results_dir+'/'+'results.json'))
    result = json.dumps(final_dict_result)
    file = open(results_dir+'/'+'results.json','w')
    file.write(result)
    file.close()
    print('JSON file has been saved.')



if __name__ == '__main__':

    with open("config.yml", 'r') as ymlfile:
        cfg = yaml.safe_load(ymlfile)

    path = os.getcwd()

    if not (os.path.isdir(cfg['config']['download_dir'])):
        os.mkdir(cfg['config']['download_dir'])
    if not (os.path.isdir(cfg['config']['results_dir'])):
        os.mkdir(cfg['config']['results_dir'])

    #download_dir = './downloads'
    #results_dir = './results'
    download_dir = cfg['config']['download_dir']
    results_dir = cfg['config']['results_dir']

    files = os.listdir(download_dir)

    number_of_workers = multiprocessing.cpu_count()


    download_files_main(cfg['config']['urls'])
    processing_files_main()







