FROM python:3.5.9-stretch

# Set the ENTRYPOINT to use bash
# (this is also where you’d set SHELL,
# if your version of docker supports this)

WORKDIR /app
COPY ./. ./

EXPOSE 5000

RUN apt-get update && apt-get install -y \
libpq-dev \
build-essential \
&& rm -rf /var/lib/apt/lists/*

# Use the environment.yml to create the conda environment.
RUN pip install -r requirements.txt

#RUN [“conda”,“env”,“create” ]

#CMD [ “conda activate conda-env && exec python3 gropozap-data.py” ]
ENTRYPOINT [ "python3", "grupozap-data.py" ]


# We set ENTRYPOINT, so while we still use exec mode, we don’t
# explicitly call /bin/bash
#CMD [ “conda activate conda-env && exec python3 application_name.py” ]


